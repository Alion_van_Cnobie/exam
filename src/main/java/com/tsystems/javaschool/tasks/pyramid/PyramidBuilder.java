package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {
            int height = getHeight(inputNumbers.size());
            int result[][] = new int[height][2*height-1];
            Set<Integer> numbers = new TreeSet<>(inputNumbers);
            Iterator<Integer> iterator = numbers.iterator();

            for (int i = 0; i < result.length; i++) {
                for (int j = (result[i].length-1)/2 - i; j <= (result[i].length-1)/2 + i; j+=2) {
                    result[i][j] = iterator.next();
                }
            }
            return result;
        } catch (Exception e) {
            throw new CannotBuildPyramidException();
        }
    }

    private int getHeight (int size) {
        double height = (Math.sqrt(8*size + 1) - 1)/2;
        if (Math.floor(height) - height != 0.0) throw new CannotBuildPyramidException();
        return (int)height;
    }
}
