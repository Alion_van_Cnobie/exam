package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            double calculated = calculate(statement);
            DecimalFormat df = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
            df.setMaximumFractionDigits(4);
            return df.format(calculated);
        } catch (Exception e) {
            return null;
        }
    }

    private double calculate(String exp) {
        exp = exp.trim();
        int nestingDegree = 0;
        char sign = ' ';
        int signIndex = 0;

        for (int i = 0; i < exp.length(); i++) {
            switch (exp.charAt(i)) {
                case ' ' :
                case '.' : continue;
                case '(' : nestingDegree++;
                break;
                case ')' : nestingDegree--;
                break;
                case '+' : if (nestingDegree == 0) {
                    if ("+-*/.".indexOf(exp.charAt(i+1)) != -1) throw new ArithmeticException();
                    return calculate(exp.substring(0, i)) + calculate(exp.substring(i+1));
                }
                break;
                case '-' : if (nestingDegree == 0) {
                    if (i == 0) continue;
                    sign = '-';
                    signIndex = i;
                }
                break;
                case '*' : if (nestingDegree == 0 && (sign == ' ' || sign == '/')) {
                    sign = '*';
                    signIndex = i;
                }
                break;
                case '/' : if (nestingDegree == 0 && (sign == ' ' || sign == '/')) {
                    sign = '/';
                    signIndex = i;
                }
                break;
                default: if (!Character.isDigit(exp.charAt(i))) throw new ArithmeticException();
            }
            if (nestingDegree < 0) throw new ArithmeticException();
        }

        if (sign == '-') {
            if ("+-*/.".indexOf(exp.charAt(signIndex+1)) != -1) throw new ArithmeticException();
            return calculate(exp.substring(0, signIndex)) - calculate(exp.substring(signIndex+1));
        }

        if (sign == '*') {
            if ("+-*/.".indexOf(exp.charAt(signIndex+1)) != -1) throw new ArithmeticException();
            return calculate(exp.substring(0, signIndex)) * calculate(exp.substring(signIndex+1));
        }

        if (sign == '/') {
            if ("+-*/.".indexOf(exp.charAt(signIndex+1)) != -1) throw new ArithmeticException();
            double rightOperand = calculate(exp.substring(signIndex+1));
            if (rightOperand == 0.0) throw new ArithmeticException();
            return calculate(exp.substring(0, signIndex)) / rightOperand;
        }

        if (exp.charAt(0) == '(') {
            if (exp.charAt(exp.length()-1) != ')') throw new ArithmeticException();
            return calculate(exp.substring(1, exp.length()-1));
        }

        if (exp.charAt(0) == '-') {
            if (exp.charAt(1) != '(' || !Character.isDigit(exp.charAt(1))) throw new ArithmeticException();
            return -1 * calculate(exp.substring(1));
        }

        return Double.parseDouble(exp);
    }
}
